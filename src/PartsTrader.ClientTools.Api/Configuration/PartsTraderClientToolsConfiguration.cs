﻿using System;
using PartsTrader.ClientTools.Api.Configuration;

namespace PartsTrader.ClientTools.Api.Configuration
{
    /// <summary>
    /// Configuration that is required by PartsTrader.ClientTools
    /// </summary>
    public class PartsTraderClientToolsConfiguration
    {
        public string PathToExclusionsJSON { get; set; }
    }
}
