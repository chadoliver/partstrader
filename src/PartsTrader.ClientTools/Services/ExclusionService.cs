﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using Microsoft.Extensions.Options;
using PartsTrader.ClientTools.Api.Configuration;
using PartsTrader.ClientTools.Api.Models;

namespace PartsTrader.ClientTools.Services
{
    public class ExclusionService : IExclusionService
    {
        private HashSet<PartNumber> _exclusions;

        public ExclusionService(IOptions<PartsTraderClientToolsConfiguration> configuration)
        {
            _exclusions = ReadData(configuration.Value.PathToExclusionsJSON);
        }

        public bool PartIsExcluded(PartNumber partNumber)
        {
            return _exclusions.Contains(partNumber);
        }

        private HashSet<PartNumber> ReadData(string path)
        {
            string json = File.ReadAllText(path);
            var partNumbers = JsonSerializer.Deserialize<IEnumerable<string>>(json)
                .Select(element => (PartNumber)element);

            return new HashSet<PartNumber>(partNumbers);
        }
    }
}
