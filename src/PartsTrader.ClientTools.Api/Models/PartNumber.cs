﻿using System;
using System.Text.RegularExpressions;

namespace PartsTrader.ClientTools.Api.Models
{
    public struct PartNumber : IPartNumber, IEquatable<PartNumber>
    {
        public PartNumber(string value)
        {
            if (!IsValid(value))
            {
                throw new InvalidPartException($"{value} is not a valid part number");
            }

            Value = value;
        }

        public string Value { get; }

        public string NormalisedValue
        {
            get => Value.ToLower();
        }

        public bool Equals(PartNumber other)
        {
            return Value.Equals(other.Value, StringComparison.OrdinalIgnoreCase);
        }

        public override string ToString()
        {
            return Value;
        }

        public override bool Equals(object other)
        {
            if (ReferenceEquals(null, other))
            {
                return false;
            }
            else if (other.GetType() != GetType())
            {
                return false;
            }
            else
            {
                return Equals((PartNumber)other);
            }
        }

        public override int GetHashCode()
        {
            return Value.ToLower().GetHashCode();
        }

        public static bool operator ==(PartNumber partNumberA, PartNumber partNumberB)
        {
            return partNumberA.Equals(partNumberB);
        }

        public static bool operator !=(PartNumber partNumberA, PartNumber partNumberB)
        {
            return !(partNumberA == partNumberB);
        }

        public static bool IsValid(string candidate)
        {
            var pattern = @"^[0-9]{4}-[0-9a-zA-Z]{4,}$";
            return Regex.IsMatch(candidate, pattern, RegexOptions.IgnoreCase);
        }

        public static implicit operator PartNumber(string s)
        {
            return new PartNumber(s);
        }

        public static implicit operator string(PartNumber partNumber)
        {
            return partNumber.ToString();
        }
    }
}
