﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Moq;
using PartsTrader.ClientTools.Services;
using PartsTrader.ClientTools.Api.Models;
using PartsTrader.ClientTools.Integration;
using System.Collections.Generic;
using System.Linq;
using PartsTrader.ClientTools.Api;
using PartsTrader.ClientTools.Models;

namespace PartsTrader.ClientTools.Tests
{
    /// <summary>
    /// Tests for <see cref="PartCatalogue" />.
    /// </summary>
    [TestClass]
    public class PartCatalogueTests
    {
        [TestInitialize]
        public void Initialise() { }

        [TestMethod]
        public void ReturnsEmptyIEnumerableForExcludedPart()
        {
            // Given that we've got an initialised instance of PartCatalogue
            Mock<IExclusionService> mockExclusionService = new Mock<IExclusionService>();
            Mock<IPartsTraderPartsService> mockPartsService = new Mock<IPartsTraderPartsService>();
            PartCatalogue catalogue = new PartCatalogue(mockPartsService.Object, mockExclusionService.Object);

            // Given that the PartNumber will be excluded by the ExclusionService
            var excludedPart = new PartNumber("1111-aaaa");
            mockExclusionService.Setup(c => c.PartIsExcluded(excludedPart)).Returns(true);

            // When we call GetCompatibleParts with the excluded PartNumber
            IEnumerable<IPartSummary> result = catalogue.GetCompatibleParts(excludedPart);

            // Then GetCompatibleParts should return an empty IEnumerable without calling PartsTraderPartsService. 
            Assert.AreEqual(result.Count(), 0);
            mockPartsService.Verify(partsService => partsService.FindAllCompatibleParts(excludedPart), Times.Never());
        }

        [TestMethod]
        public void ReturnsDataFromPartsServiceWhenPartIsNotExcluded()
        {
            // Given that we've got an initialised instance of PartCatalogue
            Mock<IExclusionService> mockExclusionService = new Mock<IExclusionService>();
            Mock<IPartsTraderPartsService> mockPartsService = new Mock<IPartsTraderPartsService>();
            PartCatalogue catalogue = new PartCatalogue(mockPartsService.Object, mockExclusionService.Object);

            // Given that the PartNumber will not be excluded by the ExclusionService
            var excludedPart = new PartNumber("1111-aaaa");
            mockExclusionService.Setup(c => c.PartIsExcluded(excludedPart)).Returns(false);

            // Given that the PartsTraderPartsService will return a specific set of results
            var expectedResult = new List<PartSummary>
            {
                new PartSummary(new PartNumber("1111-aaaa"), "Blue Combobulator"),
                new PartSummary(new PartNumber("1111-aaab"), "Red Combobulator"),
                new PartSummary(new PartNumber("6666-maycauseinjury"), "Knockoff Combobulator")
            };
            mockPartsService.Setup(partsService => partsService.FindAllCompatibleParts(excludedPart)).Returns(expectedResult);

            // When we call GetCompatibleParts with the PartNumber
            IEnumerable<IPartSummary> actualResult = catalogue.GetCompatibleParts(excludedPart);

            // Then GetCompatibleParts should return an IEnumerable with the expected results
            Assert.AreEqual(actualResult, expectedResult);
            mockPartsService.Verify(partsService => partsService.FindAllCompatibleParts(excludedPart), Times.Once());
        }
    }
}
