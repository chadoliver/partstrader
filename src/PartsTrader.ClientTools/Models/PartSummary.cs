﻿using PartsTrader.ClientTools.Api.Models;

namespace PartsTrader.ClientTools.Models
{
    public class PartSummary: IPartSummary
    {

        public PartSummary(PartNumber partNumber, string description)
        {
            PartNumber = partNumber;
            Description = description;
        }

        public PartNumber PartNumber { get; set; }

        public string Description { get; set; }
    }
}
