﻿using System;
using Microsoft.Extensions.DependencyInjection;
using PartsTrader.ClientTools.Integration;

namespace PartsTrader.ClientTools.PartsService.IoC
{
    public static class IoCExtensions
    {
        public static IServiceCollection UsePartsTraderPartsService(this IServiceCollection services)
        {
            services.AddSingleton<IPartsTraderPartsService, PartsTraderPartsService>();

            return services;
        }
    }
}
