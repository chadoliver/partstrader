﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.Options;
using PartsTrader.ClientTools.Services;
using PartsTrader.ClientTools.Api.Models;
using PartsTrader.ClientTools.Api.Configuration;

namespace PartsTrader.ClientTools.Tests
{
    /// <summary>
    /// Tests for <see cref="ExclusionService" />.
    /// </summary>
    [TestClass]
    public class ExclusionServiceTests
    {
        [TestMethod]
        public void ReturnsTrueIfPartNumberIsInExclusionFile()
        {
            ExclusionService exclusionService = new ExclusionService(GetConfigurationData());

            bool result = exclusionService.PartIsExcluded(new PartNumber("9999-charge"));
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void ReturnsFalseIfPartNumberIsNotInExclusionFile()
        {
            ExclusionService exclusionService = new ExclusionService(GetConfigurationData());

            bool result = exclusionService.PartIsExcluded(new PartNumber("1234-Nonexistent"));
            Assert.IsFalse(result);
        }

        private IOptions<PartsTraderClientToolsConfiguration> GetConfigurationData()
        {
            return Options.Create(new PartsTraderClientToolsConfiguration()
            {
                PathToExclusionsJSON = "TestData/Exclusions.json"
            });
        }
    }
}
