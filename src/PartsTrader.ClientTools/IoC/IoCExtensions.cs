﻿using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PartsTrader.ClientTools.Api;
using PartsTrader.ClientTools.Api.Configuration;
using PartsTrader.ClientTools.Services;

namespace PartsTrader.ClientTools.IoC
{
    public static class IoCExtensions
    {
        /// <summary>
        /// An optional helper method for configuring PartsTrader.ClientTools
        /// </summary>
        public static IServiceCollection UsePartsTraderClientToolsServices(
            this IServiceCollection services,
            IConfigurationSection configurationSection)
        {
            services.Configure<PartsTraderClientToolsConfiguration>(options => configurationSection.Bind(options));

            services.AddSingleton<IExclusionService, ExclusionService>();
            services.AddSingleton<IPartCatalogue, PartCatalogue>();

            return services;
        }
    }
}
