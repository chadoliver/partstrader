﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PartsTrader.ClientTools.Api.Models;
using PartsTrader.ClientTools.Api;

namespace PartsTrader.ClientTools.Tests
{
    /// <summary>
    /// Tests for <see cref="PartNumber" />.
    /// </summary>
    [TestClass]
    public class PartNumberTests
    {

        [TestMethod]
        public void IsValidReturnsFalseForAnEmptyString()
        {
            Assert.IsFalse(PartNumber.IsValid(""));
        }

        [TestMethod]
        public void IsValidReturnsTrueForARepresentativeValidValue()
        {
            Assert.IsTrue(PartNumber.IsValid("1111-a12bz9"));
        }

        [TestMethod]
        public void IsValidReturnsFalseIfThePartIdContainsAnythingNotALatinDigit()
        {
            Assert.IsFalse(PartNumber.IsValid("1a11-a12bz9"));
            Assert.IsFalse(PartNumber.IsValid("111@-a12bz9"));
            Assert.IsFalse(PartNumber.IsValid("11 1-a12bz9"));
            Assert.IsFalse(PartNumber.IsValid("11 1-a12bz9"));
            Assert.IsFalse(PartNumber.IsValid("三111-a12bz9")); // Chinese number 3
        }

        [TestMethod]
        public void IsValidReturnsFalseIfThePartIdContainsTheWrongNumberOfCharacters()
        {
            Assert.IsFalse(PartNumber.IsValid("111-a12bz9"));
            Assert.IsFalse(PartNumber.IsValid("11111-a12bz9"));
        }

        [TestMethod]
        public void IsValidReturnsFalseIfTheDashIsMissing()
        {
            Assert.IsFalse(PartNumber.IsValid("1111a12bz9"));
        }

        [TestMethod]
        public void IsValidReturnsFalseIfTheDashIsAnyOtherCharacter()
        {
            Assert.IsFalse(PartNumber.IsValid("1111_a12bz9"));
            Assert.IsFalse(PartNumber.IsValid("1111+a12bz9"));
            Assert.IsFalse(PartNumber.IsValid("1111.a12bz9"));
        }

        [TestMethod]
        public void IsValidReturnsTrueIfThePartCodeIsFourOrMoreAlphanumericCharacters()
        {
            Assert.IsFalse(PartNumber.IsValid("1111-a12"));
            Assert.IsTrue(PartNumber.IsValid("1111-ab59"));
            Assert.IsTrue(PartNumber.IsValid("1111-aB59z"));
            Assert.IsTrue(PartNumber.IsValid("1111-AU59aZ2"));
            Assert.IsTrue(PartNumber.IsValid("1111-ab59994832chwi28s"));
        }

        [TestMethod]
        public void IsValidReturnsFalseIfThePartCodeContainsNonAlphanumericCharacters()
        {
            Assert.IsFalse(PartNumber.IsValid("1111-a12ä")); // alphanumeric, but not in [A-Za-z]
            Assert.IsFalse(PartNumber.IsValid("1111-a12!"));
            Assert.IsFalse(PartNumber.IsValid("1111-ab59 "));
            Assert.IsFalse(PartNumber.IsValid("1111-ab59-"));
            Assert.IsFalse(PartNumber.IsValid("1111--ab59aw2"));
        }

        [TestMethod]
        public void ConstructorThrowsErrorForInvalidPartNumbers()
        {
            Assert.ThrowsException<InvalidPartException>(() => new PartNumber(""));
        }

        [TestMethod]
        public void PartNumbersAreEqualIfTheyHaveTheSameStringValue()
        {
            var partA = new PartNumber("1234-abcd56");
            var partB = new PartNumber("1234-abcd56");

            Assert.IsTrue(partA == partB);
            Assert.IsTrue(partA.Equals(partB));
        }

        [TestMethod]
        public void PartNumberEqualityIgnoresCase()
        {
            var partA = new PartNumber("1234-abcd56");
            var partB = new PartNumber("1234-ABCD56");
            var partC = new PartNumber("1234-abCd56");

            Assert.IsTrue(partA == partB);
            Assert.IsTrue(partB == partC);
            Assert.IsTrue(partC == partA);
        }

        [TestMethod]
        public void PartNumberHashCodeIgnoresCase()
        {
            var partA = new PartNumber("1234-abcd56");
            var partB = new PartNumber("1234-ABCD56");
            var partC = new PartNumber("1234-abCd56");

            Assert.AreEqual(partA.GetHashCode(), partB.GetHashCode());
            Assert.AreEqual(partB.GetHashCode(), partC.GetHashCode());
            Assert.AreEqual(partC.GetHashCode(), partA.GetHashCode());
        }

        [TestMethod]
        public void CanRetrieveOriginalStringUsingValueProperty()
        {
            var stringA = "1234-abcd56";
            var stringB = "1234-ABCD56";
            var partA = new PartNumber(stringA);
            var partB = new PartNumber(stringB);

            Assert.AreEqual(partA, partB);
            Assert.AreNotEqual(partA.Value, partB.Value);
            Assert.AreEqual(partA.Value, stringA);
            Assert.AreEqual(partB.Value, stringB);
        }

        [TestMethod]
        public void ToStringReturnsOriginalStringPassedToConstructor()
        {
            var stringA = "1234-abcd56";
            var stringB = "1234-ABCD56";
            var partA = new PartNumber(stringA);
            var partB = new PartNumber(stringB);

            Assert.AreEqual(partA, partB);
            Assert.AreNotEqual(partA.ToString(), partB.ToString());
            Assert.AreEqual(partA.ToString(), stringA);
            Assert.AreEqual(partB.ToString(), stringB);
        }

        [TestMethod]
        public void CanImplicitlyConvertBetweenPartNumberAndString()
        {
            string initialString = "1234-abcd56";
            PartNumber implicitlyConvertedPartNumber = initialString;
            Assert.IsInstanceOfType(implicitlyConvertedPartNumber, typeof(PartNumber));

            string finalString = implicitlyConvertedPartNumber;
            Assert.IsInstanceOfType(finalString, typeof(string));
        }

        [TestMethod]
        public void NormalisedValuesAreEqualWhenThePartNumberIsEqual()
        {
            var stringA = "1234-abcd56";
            var stringB = "1234-ABCD56";
            var partA = new PartNumber(stringA);
            var partB = new PartNumber(stringB);

            Assert.AreEqual(partA, partB);
            Assert.AreEqual(partA.NormalisedValue, partB.NormalisedValue);
        }
    }
}
