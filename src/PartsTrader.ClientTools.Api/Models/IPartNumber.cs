﻿using System;

namespace PartsTrader.ClientTools.Api.Models
{
    /// <summary>
    /// Domain model for a part number, with appropriate equality comparisons
    /// </summary>
    public interface IPartNumber : IEquatable<PartNumber>
    {
        /// <summary>
        /// The string value that was passed into the constructor
        /// </summary>
        string Value { get; }

        /// <summary>
        /// A normalised form of the string value that was passed into the constructor
        /// </summary>
        string NormalisedValue { get; }
    }
}
