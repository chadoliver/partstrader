﻿using PartsTrader.ClientTools.Api.Models;
using PartsTrader.ClientTools.Models;
using PartsTrader.ClientTools.Integration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;

namespace PartsTrader.ClientTools.PartsService
{
    /// <summary>
    /// A bare-bones implementation of the IPartsTraderPartsService interface, so that we can 
    /// serve this data through a REST API.
    /// </summary>
    public class PartsTraderPartsService : IPartsTraderPartsService
    {
        // A dictionary is not a very good data structure here, because we can't use PartNumbers as
        // the keys, so we can't correctly handle the same part number in different cases.
        private Dictionary<string, IEnumerable<PartSummary>> _data;

        public PartsTraderPartsService()
        {
            // We're hard-coding the data because this is just a bare-bones implementation.
            _data = new Dictionary<string, IEnumerable<PartSummary>>
            {
                {"1111-aaaa", new List<PartSummary> {
                    new PartSummary("1111-Fender", "Bender"),
                    new PartSummary("1234-bumpercup", "Cup holder attached to the bumper")
                }}
            };
        }

        public IEnumerable<PartSummary> FindAllCompatibleParts(PartNumber partNumber)
        {
            IEnumerable<PartSummary> compatibleParts;
            if (_data.TryGetValue(partNumber.NormalisedValue, out compatibleParts))
            {
                return compatibleParts;
            } 
            else
            {
                return Enumerable.Empty<PartSummary>();
            }
        }
    }
}
