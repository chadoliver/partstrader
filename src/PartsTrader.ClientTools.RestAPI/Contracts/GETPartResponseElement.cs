﻿using PartsTrader.ClientTools.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PartsTrader.ClientTools.RestAPI.Contracts
{
    public class GETPartResponseElement
    {
        public GETPartResponseElement(PartNumber partNumber, string description)
        {
            PartNumber = partNumber;
            Description = description;
        }

        public GETPartResponseElement(IPartSummary partSummary)
        {
            PartNumber = partSummary.PartNumber;
            Description = partSummary.Description;
        }

        public string PartNumber { get; set; }

        public string Description { get; set; }
    }
}
