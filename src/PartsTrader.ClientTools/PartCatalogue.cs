﻿using System;
using System.Collections.Generic;
using System.Linq;
using PartsTrader.ClientTools.Api;
using PartsTrader.ClientTools.Api.Models;
using PartsTrader.ClientTools.Services;
using PartsTrader.ClientTools.Integration;

namespace PartsTrader.ClientTools
{
    public class PartCatalogue : IPartCatalogue
    {
        private readonly IPartsTraderPartsService _partsService;
        private readonly IExclusionService _exlusionService;

        public PartCatalogue(
            IPartsTraderPartsService partsService,
            IExclusionService exclusionService)
        {
            _partsService = partsService;
            _exlusionService = exclusionService;
        }

        public IEnumerable<IPartSummary> GetCompatibleParts(PartNumber partNumber)
        {
            if (_exlusionService.PartIsExcluded(partNumber))
            {
                return Enumerable.Empty<IPartSummary>();
            }

            return _partsService.FindAllCompatibleParts(partNumber);
        }
    }
}