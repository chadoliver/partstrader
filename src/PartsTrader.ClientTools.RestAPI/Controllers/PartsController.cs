﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PartsTrader.ClientTools.Api;
using PartsTrader.ClientTools.Api.Models;
using PartsTrader.ClientTools.RestAPI.Contracts;

namespace PartsTrader.ClientTools.RestAPI.Controllers
{
    [ApiController]
    [Route("parts")]
    public class PartsController : ControllerBase
    {
        private readonly ILogger<PartsController> _logger;
        private readonly IPartCatalogue _partCatalogue;

        public PartsController(
            ILogger<PartsController> logger,
            IPartCatalogue partCatalogue)
        {
            _logger = logger;
            _partCatalogue = partCatalogue;
        }

        [HttpGet("{partNumber}", Name = "Part")]
        public IEnumerable<GETPartResponseElement> Get(string partNumber)
        {
            return _partCatalogue.GetCompatibleParts(new PartNumber(partNumber))
                .Select(part => new GETPartResponseElement(part));
        }
    }
}
