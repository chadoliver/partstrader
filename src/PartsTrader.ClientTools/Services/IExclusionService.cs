﻿using System;
using PartsTrader.ClientTools.Api.Models;

namespace PartsTrader.ClientTools.Services
{
    public interface IExclusionService
    {
        bool PartIsExcluded(PartNumber partNumber);
    }
}
