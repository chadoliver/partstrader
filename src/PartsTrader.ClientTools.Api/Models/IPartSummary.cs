﻿namespace PartsTrader.ClientTools.Api.Models
{
    /// <summary>
    /// Provides summary details of a part.
    /// </summary>
    public interface IPartSummary
    {
        /// <summary>
        /// The part number.
        /// </summary>
        PartNumber PartNumber { get; set; }

        /// <summary>
        /// The description of the part.
        /// </summary>
        string Description { get; set; }
    }
}